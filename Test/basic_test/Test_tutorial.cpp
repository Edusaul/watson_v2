//
// Created by eduardo on 25/06/18.
//

#include <include/gtest/gtest.h>
#include <Hadronic_current_CT.h>
#include <One_Hcurrent_process_CS.h>

class QueHacesDividiendoPorCeroException: public std::exception {

public:
    QueHacesDividiendoPorCeroException() {}

};

class Suma {

    int x, y;

public:
    Suma(int x, int y):x(x),y(y) { }

    int sumar() { return x + y; }
    int multiplicar() {

        int res = 0;
        for(int i = y; i > 0; i--) {
            res += x;
        }

        return res;
    }
    float dividir() {

        if(y == 0)
            throw QueHacesDividiendoPorCeroException();

        return x / y;
    }
};


TEST(test_tuto, test_2){

    // Crear el mundo
    Suma *s = new Suma(3, 5);

    // Hacemos las acciones correspondientes
    // Hacemos las comprobaciones
    EXPECT_EQ(8, s->sumar());
}

TEST(test_tuto, tambien_multiplica) {
    Suma *s = new Suma(3, 5);

    EXPECT_EQ(15, s->multiplicar());
}

TEST(test_tuto, incluso_divide) {
    Suma *s = new Suma(10, 5);

    EXPECT_EQ(2, s->dividir());

    s = new Suma(10, 0);

    EXPECT_THROW(s->dividir(), QueHacesDividiendoPorCeroException);
}
