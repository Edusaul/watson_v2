//
// Created by edusaul on 26/06/18.
//

#include <include/gtest/gtest.h>
#include <Hadronic_current_CT.h>
#include <One_Hcurrent_process_CS.h>
#include <Hadronic_current_CrLambda.h>
#include <Hadronic_current_KP.h>
#include <Hadronic_current_Pi.h>
#include <Hadronic_current_CrSigma.h>
#include <Hadronic_current_eta.h>

struct cross_section_CT_Test : testing::Test {
    std::string process;
//    Hadronic_current_CT *jmu_CT;
//    Hadronic_current_CrLambda *jmu_CrLambda;
//    Hadronic_current_CrSigma *jmu_CrSigma;
//    Hadronic_current_eta *jmu_eta;
//    Hadronic_current_KP *jmu_KP;
//    Hadronic_current_Pi *jmu_Pi;
    vector<Hadronic_current_base*> jmu;
    vector<Hadronic_current_base*> jmu_CT;
    One_Hcurrent_process_CS *Integrated_CS_CT;
    One_Hcurrent_process_CS *Integrated_CS_full;
    DiracSpinor_modified u_p;
    DiracSpinor_modified u_pp;

    cross_section_CT_Test() {
        double global_dipole_mass = 1.0; //GeV
        this->process="pp";

        jmu_CT.push_back(new Hadronic_current_CT(this->process, &this->u_p, &this->u_pp));
        Integrated_CS_CT = new One_Hcurrent_process_CS(&this->jmu_CT, global_dipole_mass);

        jmu.push_back(new Hadronic_current_CT(this->process, &this->u_p, &this->u_pp));
        jmu.push_back(new Hadronic_current_CrLambda(this->process, &this->u_p, &this->u_pp));
        jmu.push_back(new Hadronic_current_CrSigma(this->process, &this->u_p, &this->u_pp));
        jmu.push_back(new Hadronic_current_eta(this->process, &this->u_p, &this->u_pp));
        jmu.push_back(new Hadronic_current_KP(this->process, &this->u_p, &this->u_pp));
        jmu.push_back(new Hadronic_current_Pi(this->process, &this->u_p, &this->u_pp));

        Integrated_CS_full = new One_Hcurrent_process_CS(&this->jmu, global_dipole_mass);
    }

    ~cross_section_CT_Test() override {
        for (int i = 0; i < jmu.size(); ++i) {
            delete jmu[i];
        }
        delete Integrated_CS_CT;
        delete Integrated_CS_full;
    }
};



TEST_F(cross_section_CT_Test, _Rafi_Result_CT_pp_1571MeV) {
    EXPECT_DOUBLE_EQ(2.03605456965104e-41, Integrated_CS_CT->Call_Fnc(1.57143));
}

TEST_F(cross_section_CT_Test, _Rafi_Result_full_pp_2000MeV) {
    EXPECT_NEAR(3.75459e-41, Integrated_CS_full->Call_Fnc(2.0),0.0001);
}