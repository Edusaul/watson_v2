//
// Created by eduardo on 29/05/18.
//

#include <Parameters_GeV.h>
#include "Hadronic_current_KP.h"
#include "DiracGamma_mu.h"

Matrix<Tensor<complex<double> > > &Hadronic_current_KP::getJmu(Spin spin_N, Spin spin_Np) {

    this->jmu=this->pre_factor*Bar((*u_pp)(spin_Np))* (this->qsl + this->pmes_sl) *(*u_p)(spin_N)
            * this->q / (this->q2 - this->mmes2);

    return this->jmu;
}

//Hadronic_current_KP::Hadronic_current_KP(const std::string &process) {
//    this->process = process;
//    if(this->process == "nn" || "np"){
//        this->factor_A = 1.0;
//    }
//    if(this->process == "pp"){
//        this->factor_A = 2.0;
//    }
//
//    this->pre_factor = this->factor_A * Param::Vus*sqrt(2.0)/(4.0*Param::fpi);
//
//    this->im = {0.,1.};
//    this->mmes2 = Param::mk2;
//}

void Hadronic_current_KP::setMomenta(const Vector4<double> &p, const Vector4<double> &pp, const Vector4<double> &q,
                                      const Vector4<double> &pmes) {
    if(this->p != p){
        this->p = p;
        this->u_p->SetP4(this->p,Param::mp);
    }
    if(this->pp != pp){
        this->pp = pp;
        this->u_pp->SetP4(this->pp,Param::mp);
    }
    if(this->q != q){
        this->q = q;
        this->qsl = (DiracGamma_mu::get_gmu() * this->q);
        this->q2 = this->q * this->q;
    }

    this->pmes = pmes;
    this->pmes_sl = (DiracGamma_mu::get_gmu() * this->pmes);
}

Hadronic_current_KP::Hadronic_current_KP(const string &process, DiracSpinor_modified *u_p, DiracSpinor_modified *u_pp)
        : Hadronic_current_base(process, u_p, u_pp) {
    if(this->process == "nn" || "np"){
        this->factor_A = 1.0;
    }
    if(this->process == "pp"){
        this->factor_A = 2.0;
    }

    this->pre_factor = this->factor_A * Param::Vus*sqrt(2.0)/(4.0*Param::fpi);

    this->im = {0.,1.};
    this->mmes2 = Param::mk2;
}
