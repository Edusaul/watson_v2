//
// Created by eduardo on 17/05/18.
//

#ifndef WATSON_V2_HADRONIC_CURRENT_CT_H
#define WATSON_V2_HADRONIC_CURRENT_CT_H

#include "relativistic-quantum-mechanics.h"
#include "Hadronic_current_base.h"

class Hadronic_current_CT : public Hadronic_current_base {
protected:
    double factor_B;
public:
//    explicit Hadronic_current_CT(const string &process);

    Hadronic_current_CT(const string &process, DiracSpinor_modified *u_p, DiracSpinor_modified *u_pp);

    Matrix<Tensor<complex<double> > > &getJmu(Spin, Spin) override ;
    Matrix<Tensor<complex<double> > > &getJmu_v(Spin, Spin) override ;
    Matrix<Tensor<complex<double> > > &getJmu_a(Spin, Spin) override ;

    void setMomenta(const Vector4<double> &p, const Vector4<double> &pp, const Vector4<double> &q
            , const Vector4<double> &pmes) override;
};


#endif //WATSON_V2_HADRONIC_CURRENT_CT_H
