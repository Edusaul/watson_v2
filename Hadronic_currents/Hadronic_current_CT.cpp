//
// Created by eduardo on 17/05/18.
//

#include <Parameters_GeV.h>
#include "Hadronic_current_CT.h"
#include "DiracGamma_mu.h"
//#include "Spinor_Pool.h"

Matrix<Tensor<complex<double> > > &Hadronic_current_CT::getJmu(Spin spin_N, Spin spin_Np) {

    this->jmu=-this->pre_factor *Bar((*u_pp)(spin_Np))*(DiracGamma_mu::get_gmu()
            + this->factor_B*DiracGamma_mu::get_gmu_x_g5())*(*u_p)(spin_N);

    return this->jmu;
}

Matrix<Tensor<complex<double> > > &Hadronic_current_CT::getJmu_v(Spin spin_N, Spin spin_Np) {

    this->jmu_v = this->pre_factor *Bar((*u_pp)(spin_Np))*DiracGamma_mu::get_gmu()*(*u_p)(spin_N);

    return this->jmu_v;
}

Matrix<Tensor<complex<double> > > &Hadronic_current_CT::getJmu_a(Spin spin_N, Spin spin_Np) {

    this->jmu_a = this->pre_factor *Bar((*u_pp)(spin_Np))*this->factor_B*DiracGamma_mu::get_gmu_x_g5()*(*u_p)(spin_N);

    return this->jmu_a;
}


void Hadronic_current_CT::setMomenta(const Vector4<double> &p, const Vector4<double> &pp, const Vector4<double> &q,
                                     const Vector4<double> &pmes) {
    if(this->p != p){
        this->p = p;
        this->u_p->SetP4(this->p,Param::mp);
    }
    if(this->pp != pp){
        this->pp = pp;
        this->u_pp->SetP4(this->pp,Param::mp);
    }
}

Hadronic_current_CT::Hadronic_current_CT(const string &process, DiracSpinor_modified *u_p, DiracSpinor_modified *u_pp)
        : Hadronic_current_base(process, u_p, u_pp) {
    if(this->process == "nn"){
        this->factor_A = 1.0;
        this->factor_B = Param::D_chiPT - Param::F_chiPT;
    }
    if(this->process == "pp"){
        this->factor_A = 2.0;
        this->factor_B = -Param::F_chiPT;
    }
    if(this->process == "np"){
        this->factor_A = 1.0;
        this->factor_B = -Param::D_chiPT - Param::F_chiPT;
    }

    this->pre_factor = this->factor_A * Param::Vus*sqrt(2.0)/(2.0*Param::fpi);
}

//Hadronic_current_CT::Hadronic_current_CT(const std::string &process) {
//    this->process = process;
//    if(this->process == "nn"){
//        this->factor_A = 1.0;
//        this->factor_B = Param::D_chiPT - Param::F_chiPT;
//    }
//    if(this->process == "pp"){
//        this->factor_A = 2.0;
//        this->factor_B = -Param::F_chiPT;
//    }
//    if(this->process == "np"){
//        this->factor_A = 1.0;
//        this->factor_B = -Param::D_chiPT - Param::F_chiPT;
//    }
//
//    this->pre_factor = this->factor_A * Param::Vus*sqrt(2.0)/(2.0*Param::fpi);
//
//}
