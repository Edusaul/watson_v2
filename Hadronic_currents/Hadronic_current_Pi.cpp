//
// Created by eduardo on 29/05/18.
//

#include <Parameters_GeV.h>
#include "DiracGamma_mu.h"
#include "Hadronic_current_Pi.h"


Matrix<Tensor<complex<double> > > &Hadronic_current_Pi::getJmu(Spin spin_N, Spin spin_Np) {

    this->jmu=this->pre_factor /((this->q - this->pmes)*(this->q - this->pmes) - this->mpi2)
            *Bar((*u_pp)(spin_Np)) * DiracGamma_mu::get_g5() * (this->q - 2.0*this->pmes) *(*u_p)(spin_N);

    return this->jmu;
}

//Hadronic_current_Pi::Hadronic_current_Pi(const std::string &process) {
//    this->process = process;
//    if(this->process == "nn"){
//        this->factor_A = 1.0;
//    }else if(this->process == "pp"){
//        this->factor_A = -1.0;
//    }else if(this->process == "np"){
//        this->factor_A = -2.0;
//    }
//
//    this->pre_factor = this->factor_A * Param::Vus* (Param::D_chiPT + Param::F_chiPT) *sqrt(2.0)/(2.0*Param::fpi)*Param::mp;
//
//    this->im = {0.,1.};
//    this->mpi2 = Param::mpi2;
//}

void Hadronic_current_Pi::setMomenta(const Vector4<double> &p, const Vector4<double> &pp, const Vector4<double> &q,
                                     const Vector4<double> &pmes) {
    if(this->p != p){
        this->p = p;
        this->u_p->SetP4(this->p,Param::mp);
    }
    if(this->pp != pp){
        this->pp = pp;
        this->u_pp->SetP4(this->pp,Param::mp);
    }
    if(this->q != q){
        this->q = q;
    }

    this->pmes = pmes;
}

Hadronic_current_Pi::Hadronic_current_Pi(const string &process, DiracSpinor_modified *u_p, DiracSpinor_modified *u_pp)
        : Hadronic_current_base(process, u_p, u_pp) {
    if(this->process == "nn"){
        this->factor_A = 1.0;
    }else if(this->process == "pp"){
        this->factor_A = -1.0;
    }else if(this->process == "np"){
        this->factor_A = -2.0;
    }

    this->pre_factor = this->factor_A * Param::Vus* (Param::D_chiPT + Param::F_chiPT) *sqrt(2.0)/(2.0*Param::fpi)*Param::mp;

    this->im = {0.,1.};
    this->mpi2 = Param::mpi2;
}
