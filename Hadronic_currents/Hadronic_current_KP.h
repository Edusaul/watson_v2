//
// Created by eduardo on 29/05/18.
//

#ifndef WATSON_V2_HADRONIC_CURRENT_KP_H
#define WATSON_V2_HADRONIC_CURRENT_KP_H

#include "Hadronic_current_base.h"

class Hadronic_current_KP : public Hadronic_current_base {
protected:
    std::complex<double> im;
    Matrix<Tensor<complex<double> > > pmes_sl;
    Matrix<Tensor<complex<double> > > qsl;
    double q2;
    double mmes2;

public:
//    explicit Hadronic_current_KP(const string &process);

    Hadronic_current_KP(const string &process, DiracSpinor_modified *u_p, DiracSpinor_modified *u_pp);

    Matrix<Tensor<complex<double> > > &getJmu(Spin, Spin) override ;

    void setMomenta(const Vector4<double> &p, const Vector4<double> &pp, const Vector4<double> &q
            , const Vector4<double> &pmes) override;

};


#endif //WATSON_V2_HADRONIC_CURRENT_KP_H
