//
// Created by edusaul on 28/05/18.
//

#ifndef WATSON_V2_HADRONIC_CURRENT_BASE_H
#define WATSON_V2_HADRONIC_CURRENT_BASE_H

#include <DiracSpinor_modified.h>
#include "relativistic-quantum-mechanics.h"

class Hadronic_current_base {
protected:
    Vector4<double> p;
    Vector4<double> pp;
    Vector4<double> q;
    Vector4<double> pmes;
    Matrix<Tensor<complex<double> > > jmu;
    Matrix<Tensor<complex<double> > > jmu_v;
    Matrix<Tensor<complex<double> > > jmu_a;
    double factor_A; // Table I from Rafi's paper
    double pre_factor; // all the preceding factors of the current
    std::string process;

    DiracSpinor_modified *u_p;
    DiracSpinor_modified *u_pp;

public:

    Hadronic_current_base(const string &process, DiracSpinor_modified *u_p, DiracSpinor_modified *u_pp);

    virtual Matrix<Tensor<complex<double> > > &getJmu(Spin, Spin);

    virtual void setMomenta(const Vector4<double> &p, const Vector4<double> &pp, const Vector4<double> &q
            , const Vector4<double> &pmes);

    virtual Matrix<Tensor<complex<double> > > &getJmu_v(Spin, Spin);

    virtual Matrix<Tensor<complex<double> > > &getJmu_a(Spin, Spin);

    virtual void set_p_SpinorAngles(double, double);

    virtual void set_pp_SpinorAngles(double, double);

};


#endif //WATSON_V2_HADRONIC_CURRENT_BASE_H
