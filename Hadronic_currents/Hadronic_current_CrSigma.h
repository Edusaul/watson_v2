//
// Created by eduardo on 29/05/18.
//

#ifndef WATSON_V2_HADRONIC_CURRENT_CRSIGMA_H
#define WATSON_V2_HADRONIC_CURRENT_CRSIGMA_H

#include "Hadronic_current_base.h"

class Hadronic_current_CrSigma : public Hadronic_current_base {
protected:
    std::complex<double> im;
    double mSigma;
    double mSigma2;
    Matrix<Tensor<complex<double> > > psl;
    Matrix<Tensor<complex<double> > > pmes_sl;
    Matrix<Tensor<complex<double> > > qsl;
    //IdentityMatrix<complex<double> > identity_Matrix;
    Matrix<Tensor<complex<double> > > mSigma_Matrix;
    double q2;
    double mmes2;

public:
//    explicit Hadronic_current_CrSigma(const string &process);

    Hadronic_current_CrSigma(const string &process, DiracSpinor_modified *u_p, DiracSpinor_modified *u_pp);

    Matrix<Tensor<complex<double> > > &getJmu(Spin, Spin) override ;

    void setMomenta(const Vector4<double> &p, const Vector4<double> &pp, const Vector4<double> &q
            , const Vector4<double> &pmes) override;
};

#endif //WATSON_V2_HADRONIC_CURRENT_CRSIGMA_H
