//
// Created by edusaul on 28/05/18.
//

#include <Parameters_GeV.h>
#include "Hadronic_current_base.h"

Matrix<Tensor<complex<double> > > &Hadronic_current_base::getJmu(Spin, Spin) {
    return this->jmu;
}

Matrix<Tensor<complex<double> > > &Hadronic_current_base::getJmu_v(Spin, Spin) {
    return jmu_v;
}

Matrix<Tensor<complex<double> > > &Hadronic_current_base::getJmu_a(Spin, Spin) {
    return jmu_a;
}

void Hadronic_current_base::setMomenta(const Vector4<double> &p, const Vector4<double> &pp,
                                       const Vector4<double> &q, const Vector4<double> &pmes) {
    if(this->p != p){
        this->p = p;
        this->u_p->SetP4(this->p,Param::mp);
    }
    if(this->pp != pp){
        this->pp = pp;
        this->u_pp->SetP4(this->pp,Param::mp);
    }
    this->q = q;
    this->pmes = pmes;
}

Hadronic_current_base::Hadronic_current_base(const string &process, DiracSpinor_modified *u_p, DiracSpinor_modified *u_pp) : process(
        process), u_p(u_p), u_pp(u_pp) {}

void Hadronic_current_base::set_p_SpinorAngles(double theta, double phi) {
    this->u_p->setAngles(theta,phi);
}

void Hadronic_current_base::set_pp_SpinorAngles(double theta, double phi) {
    this->u_pp->setAngles(theta,phi);
}

