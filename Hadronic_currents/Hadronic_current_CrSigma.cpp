//
// Created by eduardo on 29/05/18.
//

#include <Parameters_GeV.h>
#include "Hadronic_current_CrSigma.h"
#include "DiracGamma_mu.h"

Matrix<Tensor<complex<double> > > &Hadronic_current_CrSigma::getJmu(Spin spin_N, Spin spin_Np) {

    this->jmu=this->pre_factor*Bar((*u_pp)(spin_Np))*(DiracGamma_mu::get_gmu()
                                                   + this->im * (Param::mup + 2.0*Param::mun)/(2.0*Param::mp)*DiracGamma_mu::get_sigma() * this->q
                                                   + (Param::D_chiPT - Param::F_chiPT)/3.0 *(DiracGamma_mu::get_gmu()
                                                                                                 - this->q/(this->q2 - this->mmes2) * this->qsl) * DiracGamma_mu::get_g5() )
              * (this->psl - this->pmes_sl + this->mSigma_Matrix)/((this->p - this->pmes)*(this->p - this->pmes) - this->mSigma2)
              * this->pmes_sl * DiracGamma_mu::get_g5() *(*u_p)(spin_N);

    return this->jmu;
}

//Hadronic_current_CrSigma::Hadronic_current_CrSigma(const std::string &process) {
//    this->process = process;
//    if(this->process == "nn"){
//        this->factor_A = -Param::D_chiPT + Param::F_chiPT;
//    }
//    if(this->process == "pp"){
//        this->factor_A = (-Param::D_chiPT + Param::F_chiPT)/2.0;
//    }
//    if(this->process == "np"){
//        this->factor_A = (Param::D_chiPT - Param::F_chiPT)/2.0;
//    }
//
//    this->pre_factor = this->factor_A * Param::Vus*sqrt(2.0)/(2.0*Param::fpi);
//
//    this->im = {0.,1.};
//    this->mSigma = Param::mSigma_1190;
//    this->mSigma2 = this->mSigma * this->mSigma;
//    this->mmes2 = Param::mk2;
//    IdentityMatrix<Tensor<complex<double> > > identity_Matrix(4);
//    this->mSigma_Matrix = this->mSigma*identity_Matrix;
//}

void Hadronic_current_CrSigma::setMomenta(const Vector4<double> &p, const Vector4<double> &pp, const Vector4<double> &q,
                                           const Vector4<double> &pmes) {
    if(this->p != p){
        this->p = p;
        this->u_p->SetP4(this->p,Param::mp);
        this->psl = (DiracGamma_mu::get_gmu() * this->p);
    }
    if(this->pp != pp){
        this->pp = pp;
        this->u_pp->SetP4(this->pp,Param::mp);
    }
    if(this->q != q){
        this->q = q;
        this->qsl = (DiracGamma_mu::get_gmu() * this->q);
        this->q2 = this->q * this->q;
    }

    this->pmes = pmes;
    this->pmes_sl = (DiracGamma_mu::get_gmu() * this->pmes);
}

Hadronic_current_CrSigma::Hadronic_current_CrSigma(const string &process, DiracSpinor_modified *u_p, DiracSpinor_modified *u_pp)
        : Hadronic_current_base(process, u_p, u_pp) {
    if(this->process == "nn"){
        this->factor_A = -Param::D_chiPT + Param::F_chiPT;
    }
    if(this->process == "pp"){
        this->factor_A = (-Param::D_chiPT + Param::F_chiPT)/2.0;
    }
    if(this->process == "np"){
        this->factor_A = (Param::D_chiPT - Param::F_chiPT)/2.0;
    }

    this->pre_factor = this->factor_A * Param::Vus*sqrt(2.0)/(2.0*Param::fpi);

    this->im = {0.,1.};
    this->mSigma = Param::mSigma_1190;
    this->mSigma2 = this->mSigma * this->mSigma;
    this->mmes2 = Param::mk2;
    IdentityMatrix<Tensor<complex<double> > > identity_Matrix(4);
    this->mSigma_Matrix = this->mSigma*identity_Matrix;
}
