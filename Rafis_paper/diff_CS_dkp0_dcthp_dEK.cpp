//
// Created by edusaul on 17/05/18.
//

#include <Parameters_GeV.h>
#include "diff_CS_dkp0_dcthp_dEK.h"

diff_CS_dkp0_dcthp_dEK::diff_CS_dkp0_dcthp_dEK(Integration_Class *diff_CS, double precission, int inter) : diff_CS(
        diff_CS), precission(precission), inter(inter) {this->Pi = Param::pi;}

double diff_CS_dkp0_dcthp_dEK::Call_Fnc(double Emes) {

    double phi_mes_min= 0.0;
    double phi_mes_max= 2.0 * this->Pi;

    vector<double> imputs(4);
    imputs[0] = this->k0;
    imputs[1] = this->kp0;
    imputs[2] = this->theta_p;
    imputs[3] = Emes;

    this->diff_CS->Change_other_imputs(imputs);
    return this->diff_CS->Integrate(phi_mes_min, phi_mes_max, this->precission, this->inter);
}

int diff_CS_dkp0_dcthp_dEK::Change_other_imputs(std::vector<double> imputs_k0_kp0_thp) {
    this->k0= imputs_k0_kp0_thp[0];
    this->kp0= imputs_k0_kp0_thp[1];
    this->theta_p= imputs_k0_kp0_thp[2];
    return 0;
}


