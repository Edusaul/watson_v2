//
// Created by edusaul on 16/05/18.
//

#ifndef WATSON_V2_AMPLITUDE_H
#define WATSON_V2_AMPLITUDE_H

#include "relativistic-quantum-mechanics.h"
#include "Leptonic_current.h"
#include "Hadronic_current_CT.h"


class Amplitude {
private:
    Hadronic_current_base *jmu;
    Leptonic_current *lmu;

    Matrix<Tensor<complex<double> > > lmu1;
    Matrix<Tensor<complex<double> > > lmu2;

    Vector4<double> k;
    Vector4<double> kp;

public:

    Amplitude(Hadronic_current_base *jmu, Leptonic_current *lmu);

    Amplitude() = default;

    virtual double get_M2(const Vector4<double> &k, const Vector4<double> &kp, const Vector4<double> &p, const Vector4<double> &pp
            , const Vector4<double> &q, const Vector4<double> &pmes);
};


#endif //WATSON_V2_AMPLITUDE_H
