//
// Created by edusaul on 25/06/18.
//

#ifndef WATSON_V2_ONE_HCURRENT_PROCESS_H
#define WATSON_V2_ONE_HCURRENT_PROCESS_H


#include <string>
#include "Amplitude.h"
#include "Leptonic_current.h"
#include "differential_CS.h"
#include "diff_CS_dkp0_dcthp_dEK.h"
#include "Cross_Section.h"
#include "diff_CS_dkp0_dcthp.h"
#include "diff_CS_dkp0.h"
#include "Amplitude_Sum.h"

class One_Hcurrent_process_CS : public Call_Function_Base {
protected:

    vector<Hadronic_current_base*> *jmu;
    double global_dipole_mass;
    double const_factors;

    Leptonic_current lmu;
    Amplitude_Sum *M2;
    differential_CS *CS;
    Integration_Class_Gauss20 *I_phi_K;

    diff_CS_dkp0_dcthp_dEK *cs_dEk;
    Integration_Class_Gauss20 *I_dEK;

    diff_CS_dkp0_dcthp *cs_dthp;
    Integration_Class_Gauss20 *I_dthp;

    diff_CS_dkp0 *cs_dkp0;
    Integration_Class_Gauss20 *I_dkp0;

    Cross_Section *Integrated_CS;

public:
    One_Hcurrent_process_CS(vector<Hadronic_current_base*>*, double);

    ~One_Hcurrent_process_CS() override;

    double Call_Fnc(double) override;

};


#endif //WATSON_V2_ONE_HCURRENT_PROCESS_H
