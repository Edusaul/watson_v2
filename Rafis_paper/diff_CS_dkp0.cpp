//
// Created by eduardo on 18/05/18.
//

#include <Parameters_GeV.h>
#include "diff_CS_dkp0.h"

diff_CS_dkp0::diff_CS_dkp0(Integration_Class *diff_CS, double precission, int inter) : diff_CS(diff_CS), precission(precission), inter(inter) {
    this->Pi = Param::pi;
}

double diff_CS_dkp0::Call_Fnc(double kp0) {
    double theta_p_min = 0.0;
    double theta_p_max = this->Pi;

    vector<double> imputs(2);
    imputs[0] = this->k0;
    imputs[1] = kp0;

    this->diff_CS->Change_other_imputs(imputs);
    return this->diff_CS->Integrate(theta_p_min,theta_p_max, this->precission, this->inter);
}

int diff_CS_dkp0::Change_other_imputs(vector<double> imputs_k0) {
    this->k0= imputs_k0[0];
    return 0;
}


