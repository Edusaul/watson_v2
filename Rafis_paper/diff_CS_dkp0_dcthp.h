//
// Created by eduardo on 18/05/18.
//

#ifndef WATSON_V2_DIFF_CS_DKP0_DCTHP_H
#define WATSON_V2_DIFF_CS_DKP0_DCTHP_H


#include <Call_Function.h>

class diff_CS_dkp0_dcthp : public Call_Function_Base {
private:
    double k0; //MeV
    double kp0; //MeV
    Integration_Class *diff_CS;
    double precission;
    int inter;

    double mmes;

public:
    explicit diff_CS_dkp0_dcthp(Integration_Class*, double precission = 1, int inter = 1);

    double Call_Fnc(double) override;

    int Change_other_imputs(vector<double>) override;
};


#endif //WATSON_V2_DIFF_CS_DKP0_DCTHP_H
