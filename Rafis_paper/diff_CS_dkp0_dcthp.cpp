//
// Created by eduardo on 18/05/18.
//

#include <Integration.h>
#include <Parameters_GeV.h>
#include "diff_CS_dkp0_dcthp.h"

diff_CS_dkp0_dcthp::diff_CS_dkp0_dcthp(Integration_Class *diff_CS, double precission, int inter) : diff_CS(diff_CS), precission(precission), inter(inter) {
    this->mmes=Param::mk;
}

double diff_CS_dkp0_dcthp::Call_Fnc(double theta_p) {
    double Emes_min = this->mmes;
    double Emes_max = this->k0 - this->kp0;

    vector<double> imputs(3);
    imputs[0] = this->k0;
    imputs[1] = this->kp0;
    imputs[2] = theta_p;

    this->diff_CS->Change_other_imputs(imputs);

    return this->diff_CS->Integrate(Emes_min,Emes_max, this->precission, this->inter);;
}

int diff_CS_dkp0_dcthp::Change_other_imputs(vector<double> imputs_k0_kp0) {
    this->k0= imputs_k0_kp0[0];
    this->kp0= imputs_k0_kp0[1];
    return 0;
}
