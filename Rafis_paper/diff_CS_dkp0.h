//
// Created by eduardo on 18/05/18.
//

#ifndef WATSON_V2_DIFF_CS_DKP0_H
#define WATSON_V2_DIFF_CS_DKP0_H

#include <Integration.h>
#include <Call_Function.h>

class diff_CS_dkp0 : public Call_Function_Base{
private:
    double k0;
    Integration_Class *diff_CS;
    double precission;
    int inter;

    double Pi;
public:
    explicit diff_CS_dkp0(Integration_Class*, double precission = 1, int inter = 1);

    double Call_Fnc(double) override;

    int Change_other_imputs(vector<double>) override;
};


#endif //WATSON_V2_DIFF_CS_DKP0_H
