
//
// Created by edusaul on 17/05/18.
//

#include <cmath>
#include <Parameters_GeV.h>
#include "Amplitude.h"
#include "relativistic-quantum-mechanics.h"
#include "differential_CS.h"

differential_CS::differential_CS(Amplitude *M2, double MF) : M2(M2), MF(MF) {
    this->ml = Param::mmu;
    this->mmes = Param::mk;
    this->MN = Param::mp;
    this->Pi = Param::pi;
}


double differential_CS::Call_Fnc(double phi_mes) {

    double ds;
    double kpv = sqrt(this->kp0 * this->kp0 - this->ml * this->ml);
    double pmesv = sqrt(this->Emes * this->Emes - this->mmes * this->mmes);

    double sth_p = sin(this->theta_p);
    double cth_p = cos(this->theta_p);
    double th_nu = atan(kpv * sth_p / (this->k0 - kpv * cth_p));

    double sth_nu = sin(th_nu);
    double cth_nu = cos(th_nu);

    double sphi_mes = sin(phi_mes);
    double cphi_mes = cos(phi_mes);

    double q0 = this->k0 - this->kp0;
    double q2 = this->ml * this->ml - 2.0 * this->k0 * (this->kp0 - kpv * cth_p);
    double qv = sqrt(q0 * q0 - q2);

    double cth_mes = (MN*MN + qv * qv + pmesv * pmesv - (this->Emes - q0 - this->MN) * (this->Emes - q0 - this->MN))
            /(2.0 * qv * pmesv);

    if (cth_mes > -1.0 && cth_mes < 1.0) {

        double th_mes = acos(cth_mes);
        double sth_mes = sin(th_mes);

        Vector4<double> k;
        Vector4<double> kp;
        Vector4<double> p;
        Vector4<double> pp;
        Vector4<double> pmes;

        double kpv_x = kpv * (sth_p * cth_nu + cth_p * sth_nu);
        double kpv_z = kpv * (cth_p * cth_nu - sth_p * sth_nu);

        double pmesv_x = pmesv * (sth_mes * cphi_mes);
        double pmesv_y = pmesv * (sth_mes * sphi_mes);
        double pmesv_z = pmesv * cth_mes;

        k.SetP4(k0, k0 * sth_nu, 0, k0 * cth_nu);
        kp.SetP4(this->kp0, kpv_x, 0, kpv_z);
        p.SetP4(this->MN, 0, 0, 0);
        pmes.SetP4(this->Emes, pmesv_x, pmesv_y, pmesv_z);

        Vector4<double> q = k - kp;
        pp = q + p - pmes;

        double amp = M2->get_M2(k, kp, p, pp, q, pmes);

        // here we multiply by sin(theta_p) because we are integrating theta_p instead of cos(theta_p)
        ds = kpv / (512.0 * this->Pi * this->Pi * this->Pi * this->Pi * this->k0 * this->MN * qv) * amp *sin(theta_p);

        double FF = 1.0/((1.0-q2/(MF*MF))*(1.0-q2/(MF*MF)));

        ds = ds * FF * FF;

    }else ds = 0.0;



    return ds;
}

int differential_CS::Change_other_imputs(std::vector<double> imputs_k0_kp0_thp_Emes) {
    this->k0= imputs_k0_kp0_thp_Emes[0];
    this->kp0= imputs_k0_kp0_thp_Emes[1];
    this->theta_p= imputs_k0_kp0_thp_Emes[2];
    this->Emes= imputs_k0_kp0_thp_Emes[3];
    return 0;
}

