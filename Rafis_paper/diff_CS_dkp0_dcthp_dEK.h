//
// Created by edusaul on 17/05/18.
//

#ifndef WATSON_V2_DIFF_CS_DKP0_DCTHP_DEK_H
#define WATSON_V2_DIFF_CS_DKP0_DCTHP_DEK_H

#include <Integration.h>
#include <Call_Function.h>

class diff_CS_dkp0_dcthp_dEK : public Call_Function_Base {
private:
    double k0; //MeV
    double kp0; //MeV
    double theta_p;
    Integration_Class *diff_CS;
    double precission;
    int inter;

    double Pi;

public:
    explicit diff_CS_dkp0_dcthp_dEK(Integration_Class *diff_CS, double precission = 1, int inter = 1);

    double Call_Fnc(double) override;

    int Change_other_imputs(std::vector<double>) override;

};


#endif //WATSON_V2_DIFF_CS_DKP0_DCTHP_DEK_H
