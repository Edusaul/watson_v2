//
// Created by edusaul on 21/05/18.
//

#include <Parameters_GeV.h>
#include "relativistic-quantum-mechanics.h"
#include "Leptonic_current.h"
#include "DiracGamma_mu.h"
#include "Spinor_Pool.h"


Matrix<Tensor<complex<double> > > &Leptonic_current::getLmu(Spin spin_nu, Spin spin_l) {

    this->lmu = Bar(u_kp(spin_l))*(DiracGamma_mu::get_gmu()-DiracGamma_mu::get_gmu_x_g5())*u_k(spin_nu);

    return this->lmu;
}

void Leptonic_current::setMomenta(const Vector4<double> &k, const Vector4<double> &kp) {
    if(this->k != k){
        this->k = k;
        this->u_k.SetP4(this->k,0.0);
    }
    if(this->kp != kp){
        this->kp = kp;
        this->u_kp.SetP4(this->kp,Param::mmu);
    }
}