//
// Created by eduardo on 29/05/18.
//

#ifndef WATSON_V2_AMPLITUDE_SUM_H
#define WATSON_V2_AMPLITUDE_SUM_H

#include "relativistic-quantum-mechanics.h"
#include "Leptonic_current.h"
#include "Hadronic_current_CT.h"
#include "Amplitude.h"

class Amplitude_Sum : public Amplitude{
private:
    vector<Hadronic_current_base*> *jmu;
    Leptonic_current *lmu;

    Matrix<Tensor<complex<double> > > lmu1;
    Matrix<Tensor<complex<double> > > lmu2;

    Vector4<double> k;
    Vector4<double> kp;

public:
//    Amplitude_Sum(Hadronic_current_base *jmu, Leptonic_current *lmu, vector<Hadronic_current_base*> *jmu1,
//                  Leptonic_current *lmu1);

    Amplitude_Sum(vector<Hadronic_current_base*> *jmu, Leptonic_current *lmu);

    double get_M2(const Vector4<double> &k, const Vector4<double> &kp, const Vector4<double> &p, const Vector4<double> &pp
            , const Vector4<double> &q, const Vector4<double> &pmes) override;
};


#endif //WATSON_V2_AMPLITUDE_SUM_H
