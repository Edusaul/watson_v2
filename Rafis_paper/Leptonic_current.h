//
// Created by edusaul on 21/05/18.
//

#ifndef WATSON_V2_LEPTONIC_CURRENT_H
#define WATSON_V2_LEPTONIC_CURRENT_H


class Leptonic_current {
private:
    Vector4<double> k;
    Vector4<double> kp;
    Matrix<Tensor<complex<double> > > lmu;

    DiracSpinor u_k;
    DiracSpinor u_kp;
public:
    Matrix<Tensor<complex<double> > > &getLmu(Spin, Spin);

    void setMomenta(const Vector4<double> &k, const Vector4<double> &kp);
};


#endif //WATSON_V2_LEPTONIC_CURRENT_H
