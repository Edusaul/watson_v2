//
// Created by eduardo on 18/05/18.
//

#include <Integration.h>
#include <Parameters_GeV.h>
#include "Cross_Section.h"

Cross_Section::Cross_Section(Integration_Class *diff_CS, double precission, int inter) : diff_CS(diff_CS), precission(precission), inter(inter) {
    this->ml = Param::mmu;
    this->mmes = Param::mk;
}

double Cross_Section::Call_Fnc(double k0) {
    double kp0_min = this->ml;
    double kp0_max = k0 - this->mmes;

    vector<double> imputs(1);
    imputs[0] = k0;

    this->diff_CS->Change_other_imputs(imputs);
    double x=this->diff_CS->Integrate(kp0_min,kp0_max, this->precission, this->inter);
    return x;
}


