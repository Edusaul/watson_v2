//
// Created by eduardo on 29/05/18.
//

#include <Parameters_GeV.h>
#include "Amplitude_Sum.h"

double Amplitude_Sum::get_M2(const Vector4<double> &k, const Vector4<double> &kp, const Vector4<double> &p, const Vector4<double> &pp
        , const Vector4<double> &q, const Vector4<double> &pmes) {
    double intensity = 0.;

    Matrix<complex<double> > amp(1,1); // amplitude

    double const_factors=1.0;

    if(this->k != k || this->kp !=kp) {
        this->k=k;
        this->kp=kp;
        this->lmu->setMomenta(k,kp);
        this->lmu1 = this->lmu->getLmu(-1/2., -1/2.);
        this->lmu2 = this->lmu->getLmu(-1/2., 1/2.);
    }

    for (int i = 0; i < this->jmu->size(); ++i) {
        (*this->jmu)[i]->setMomenta(p, pp, q, pmes);
    }

    Tensor<complex<double>> aux_tensor;
    aux_tensor.SetRank(1);

    Matrix<Tensor<complex<double>>> jmu_sum(1,1,aux_tensor);
    jmu_sum.Zero();
    for (Spin spin_N = -1 / 2.; spin_N <= 1 / 2.; spin_N++) {
        for (Spin spin_Np = -1 / 2.; spin_Np <= 1 / 2.; spin_Np++) {
            for (int i = 0; i < this->jmu->size(); ++i) {
                jmu_sum += (*this->jmu)[i]->getJmu(spin_N, spin_Np);
            }
            amp = lmu1*jmu_sum;
            intensity += norm(amp(0, 0));
            amp = lmu2*jmu_sum;
            intensity += norm(amp(0, 0));
            jmu_sum.Zero();
        }
    }

    // average over initial spins
    intensity /= 2.0;
    return intensity*const_factors;
}

//Amplitude_Sum::Amplitude_Sum(Hadronic_current_base *jmu, Leptonic_current *lmu, vector<Hadronic_current_base*> *jmu1,
//                             Leptonic_current *lmu1) : Amplitude(jmu, lmu), jmu(jmu1), lmu(lmu1) {}

Amplitude_Sum::Amplitude_Sum(vector<Hadronic_current_base*> *jmu, Leptonic_current *lmu) : jmu(jmu), lmu(lmu) {}



