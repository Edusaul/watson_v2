#include <iostream>
#include <Integration.h>
#include <Parameters_GeV.h>
#include <Write.h>
#include <Hadronic_current_CrSigma.h>
#include <Hadronic_current_eta.h>
#include <Hadronic_current_KP.h>
#include <Hadronic_current_Pi.h>
#include <DiracSpinor_modified.h>
#include "Hadronic_current_CT.h"
#include "Amplitude.h"
#include "differential_CS.h"
#include "diff_CS_dkp0_dcthp_dEK.h"
#include "diff_CS_dkp0_dcthp.h"
#include "diff_CS_dkp0.h"
#include "Cross_Section.h"
#include "Leptonic_current.h"
#include "Hadronic_current_CrLambda.h"
#include "Amplitude_Sum.h"
#include "One_Hcurrent_process_CS.h"

int main() {
    clock_t time1,time2;
    time1= clock(); //Start timer

    std::string file = "../../Data/data.dat";

    int N = 4;
    double k0min = 0.500;
    double k0max = 2.0;
    double k0step = (k0max - k0min)/(double(N-1));

    std::vector<double> dx(static_cast<unsigned long>(N));
    std::vector<double> dy(static_cast<unsigned long>(N));

#pragma omp parallel for
    for (int i = 0; i < N; ++i) {

        Leptonic_current lmu;
        string process = "pp";
        DiracSpinor_modified u_p;
        DiracSpinor_modified u_pp;
        Hadronic_current_CT jmu_CT(process, &u_p, &u_pp);
        Hadronic_current_CrLambda jmu_CrLambda(process, &u_p, &u_pp);
        Hadronic_current_CrSigma jmu_CrSigma(process, &u_p, &u_pp);
        Hadronic_current_eta jmu_eta(process, &u_p, &u_pp);
        Hadronic_current_KP jmu_KP(process, &u_p, &u_pp);
        Hadronic_current_Pi jmu_Pi(process, &u_p, &u_pp);

//        vector<Hadronic_current_base*> jmu = {&jmu_CT, &jmu_CrLambda, &jmu_CrSigma,
//                                              &jmu_eta, &jmu_KP, &jmu_Pi};

        vector<Hadronic_current_base*> jmu = {&jmu_CT};

        double global_dipole_mass = 1.0; //GeV
        One_Hcurrent_process_CS Integrated_CS(&jmu, global_dipole_mass);

        dx[i] = k0min + k0step * i;
        dy[i] = Integrated_CS.Call_Fnc(dx[i]);

//        dx[i]=dx[i]/1000.0;

        cout<<dx[i]<<" "<<dy[i]<<endl;
    }

    Write wrt(dx,dy,file);
    wrt.write();
    wrt.gnuplot_Plot();

    //this should have the result 2.03605*10^-41 cm^2 for 1571.43 MeV
//    cout<<"1571.43 MeV ->  "<<Integrated_CS.Call_Fnc(1571.43)*const_factors<<endl;
//    cout<<"1.54 GeV ->  "<<Integrated_CS.Call_Fnc(1540.4890091886794)*const_factors<<endl;


//////////////////////////////////////////////////////////////////////////

    time2= clock(); //timer
    float diff;
    diff= ((float)time2-(float)time1)/CLOCKS_PER_SEC;
    cout<<endl<<"Time"<<" "<<diff<<" "<<"s"<<endl;

    return 0;
}


