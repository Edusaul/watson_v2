//
// Created by eduardo on 18/05/18.
//

#ifndef WATSON_V2_CROSS_SECTION_H
#define WATSON_V2_CROSS_SECTION_H

#include <Call_Function.h>

class Cross_Section : public Call_Function_Base{
private:
    double ml;
    double mmes;
    Integration_Class *diff_CS;
    double precission;
    int inter;

public:
    explicit Cross_Section(Integration_Class*, double precission = 1, int inter = 1);

    double Call_Fnc(double) override;
};


#endif //WATSON_V2_CROSS_SECTION_H
