//
// Created by edusaul on 25/06/18.
//

#include <Call_Function.h>
#include <Integration.h>
#include <Parameters_GeV.h>
#include "One_Hcurrent_process_CS.h"

One_Hcurrent_process_CS::One_Hcurrent_process_CS(vector<Hadronic_current_base*> *jmu, double global_dipole_mass) : jmu(jmu),
                                                                                                 global_dipole_mass(
                                                                                                         global_dipole_mass) {
    this->M2 = new Amplitude_Sum(this->jmu,&this->lmu);
//    Amplitude_Sum M2(&jmu_CT,&lmu,&jmu,&lmu);
    this->CS = new differential_CS(this->M2, this->global_dipole_mass);
    this->I_phi_K = new Integration_Class_Gauss20(this->CS);

    this->cs_dEk = new diff_CS_dkp0_dcthp_dEK(this->I_phi_K,1,1);
    this->I_dEK = new Integration_Class_Gauss20(this->cs_dEk);

    this->cs_dthp = new diff_CS_dkp0_dcthp(this->I_dEK,1,1);
    this->I_dthp = new Integration_Class_Gauss20(this->cs_dthp);

    this->cs_dkp0 = new diff_CS_dkp0(this->I_dthp,1,1);
    this->I_dkp0 = new Integration_Class_Gauss20(this->cs_dkp0);

    this->Integrated_CS = new Cross_Section(this->I_dkp0,1,1);

    this->const_factors = Param::Gf2/2.*Param::hccm*Param::hccm;
}

One_Hcurrent_process_CS::~One_Hcurrent_process_CS() {
    delete M2;
    delete CS;
    delete I_phi_K;
    delete cs_dEk;
    delete I_dEK;
    delete cs_dthp;
    delete I_dthp;
    delete cs_dkp0;
    delete I_dkp0;
    delete Integrated_CS;
}

double One_Hcurrent_process_CS::Call_Fnc(double energy) {
    double x=this->Integrated_CS->Call_Fnc(energy) * this->const_factors;
    return x;
}
