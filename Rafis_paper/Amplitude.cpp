//
// Created by edusaul on 16/05/18.
//

#include <Parameters_GeV.h>
#include "Amplitude.h"

double Amplitude::get_M2(const Vector4<double> &k, const Vector4<double> &kp, const Vector4<double> &p, const Vector4<double> &pp
        , const Vector4<double> &q, const Vector4<double> &pmes) {
    double intensity = 0.;

    Matrix<complex<double> > amp(1,1); // amplitude

    double const_factors=1.0;//Param::Gf2/2.;

    if(this->k != k || this->kp !=kp) {
        this->k=k;
        this->kp=kp;
        this->lmu->setMomenta(k,kp);
        this->lmu1 = this->lmu->getLmu(-1/2., -1/2.);
        this->lmu2 = this->lmu->getLmu(-1/2., 1/2.);
    }

    this->jmu->setMomenta(p, pp, q, pmes);

    for (Spin spin_N = -1 / 2.; spin_N <= 1 / 2.; spin_N++) {
        for (Spin spin_Np = -1 / 2.; spin_Np <= 1 / 2.; spin_Np++) {
            amp = lmu1*this->jmu->getJmu(spin_N, spin_Np);
            intensity += norm(amp(0, 0));
            amp = lmu2*this->jmu->getJmu(spin_N, spin_Np);
            intensity += norm(amp(0, 0));
        }
    }

    // average over initial spins
    intensity /= 2.0;
    return intensity*const_factors;
}

Amplitude::Amplitude(Hadronic_current_base *jmu, Leptonic_current *lmu) : jmu(jmu), lmu(lmu) {}


