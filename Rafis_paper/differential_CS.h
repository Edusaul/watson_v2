//
// Created by edusaul on 17/05/18.
//

#ifndef WATSON_V2_DIFFERENTIAL_CS_H
#define WATSON_V2_DIFFERENTIAL_CS_H
#include <Call_Function.h>

class differential_CS : public Call_Function_Base {
private:
    double k0; //MeV
    double kp0; //MeV
    double theta_p;
    double Emes; //MeV
    Amplitude *M2;

    double ml;
    double mmes;
    double MN;
    double Pi;
    double MF;

public:
    explicit differential_CS(Amplitude *M2, double MF);

    double Call_Fnc(double) override;

    int Change_other_imputs(std::vector<double>) override;
};


#endif //WATSON_V2_DIFFERENTIAL_CS_H
