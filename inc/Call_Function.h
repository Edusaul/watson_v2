#ifndef CALL_FUNCTION_H
#define CALL_FUNCTION_H

#include <vector>

//Base class

class Call_Function_Base {
protected:
	
	
public:
	//Constructor
	Call_Function_Base();	
	//Destructor
	virtual ~Call_Function_Base();	
	//Method
	virtual double Call_Fnc(double);
	virtual int Change_other_imputs(std::vector<double>);
};

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
//To call a double function(double)

class Call_db_func_1_db : public Call_Function_Base {
protected:
	double (*fnc)(double);
public:
	//Constructor
	Call_db_func_1_db(double (*)(double));
	//Destructor
	virtual ~Call_db_func_1_db();
	//Method
	double Call_Fnc(double);
};

class Call_db_func_2_db : public Call_Function_Base {
protected:
	double (*fnc)(double,double);
	double static_parameter;
public:
	//Constructor
	Call_db_func_2_db(double (*)(double,double),double);
	//Destructor
	virtual ~Call_db_func_2_db();
	//Method
	double Call_Fnc(double);
};


#endif // CALL_FUNCTION_H