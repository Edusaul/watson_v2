//
// Created by edusaul on 21/05/18.
//

#ifndef LIBRARIES_PLOT_H
#define LIBRARIES_PLOT_H


#include <string>

class Plot {
protected:
    std::string file_path;
    std::string plot_path;

public:
    Plot(std::string file_path, std::string plot_path);
    virtual void Math_Plot(std::string AxesLabel="{}", std::string PlotRange="All");
    virtual void gnuplot_Plot();
    virtual void Change_paths(std::string, std::string);
};


#endif //LIBRARIES_PLOT_H
