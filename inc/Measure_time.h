//
// Created by edusaul on 10/07/18.
//

#ifndef WATSON_V2_MEASURE_TIME_H
#define WATSON_V2_MEASURE_TIME_H

#include <ctime>

class Measure_time {
private:
    clock_t time1;
    clock_t time2;

public:
    Measure_time();
    void getTime();
};


#endif //WATSON_V2_MEASURE_TIME_H
