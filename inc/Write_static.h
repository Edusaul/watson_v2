//
// Created by edusaul on 11/07/18.
//

#ifndef LIBRARIES_WRITE_STATIC_H
#define LIBRARIES_WRITE_STATIC_H

#include <string>
#include <vector>

class Write_static {
public:
    static void Write_to_file (const std::string &file_path,
                           std::vector<std::vector<double>> &data);
};


#endif //LIBRARIES_WRITE_STATIC_H
