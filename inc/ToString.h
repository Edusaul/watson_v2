//
// Created by edusaul on 11/07/18.
//

#ifndef LIBRARIES_TOSTRING_H
#define LIBRARIES_TOSTRING_H

#include <sstream>

class ToString {
public:
    template <typename T>
    static std::string to_string (T);
};

template <typename T>
std::string ToString::to_string(T value) {
    std::ostringstream os ;
    os << value ;
    return os.str() ;}


#endif //LIBRARIES_TOSTRING_H
