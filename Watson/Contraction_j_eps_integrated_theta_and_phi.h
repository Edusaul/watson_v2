//
// Created by edusaul on 29/06/18.
//

#ifndef WATSON_V2_CONTRACTION_J_EPS_INTEGRATED_THETA_AND_PHI_H
#define WATSON_V2_CONTRACTION_J_EPS_INTEGRATED_THETA_AND_PHI_H

#include <Integration.h>

class Contraction_j_eps_integrated_theta_and_phi  : public Integrable<complex<double>>{
private:
    Contraction_j_eps_integrated_phi *jmu_eps_integrated_phi;
    double Q2;
    double W2;
    double r;
    double thetaMin;
    double thetaMax;
    int iter;

public:

    explicit Contraction_j_eps_integrated_theta_and_phi(Contraction_j_eps_integrated_phi *jmu_eps_integrated_phi);

    Contraction_j_eps_integrated_theta_and_phi(Contraction_j_eps_integrated_phi *jmu_eps_integrated_phi,
                                               int iter);

    int Change_other_imputs(std::vector<double>);

    complex<double> integrand(double) override;
};


#endif //WATSON_V2_CONTRACTION_J_EPS_INTEGRATED_THETA_AND_PHI_H
