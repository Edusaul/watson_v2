//
// Created by edusaul on 10/07/18.
//

#include <tclap/CmdLine.h>
#include <ToString.h>

#include "Arguments.h"

using namespace TCLAP;
using namespace std;

Arguments::Arguments(int argc, char** argv) {
    try {
        CmdLine cmd("Program to calculate chi_r_lambda", ' ', "0.1");

        ValueArg<string> filePath("","path","Set the path to write file",false, "../../Data/", "string");
        cmd.add( filePath );
        ValueArg<string> process("p","process","Select the process: \n pp -> p + nu -> p + mu + K     (default)"
                                               "\n np -> n + nu -> p + mu + K \n nn -> n + nu -> n + mu + K"
                                               ,false, "pp", "string");
        cmd.add( process );
        ValueArg<int> n_Q2("Q","nQ2","Number of points in Q2 variable (default 20 pts.)",false, 20, "int");
        cmd.add( n_Q2 );
        ValueArg<int> n_W("W","nW","Number of points in W variable (default 20 pts.)",false, 20, "int");
        cmd.add( n_W );
        ValueArg<double> Q2_min("","Q2min","Minimal value for Q2 in GeV (default 0 GeV)",false, 0.0, "double");
        cmd.add( Q2_min );
        ValueArg<double> Q2_max("","Q2max","Maximal value for Q2 in GeV (default 1.5 GeV)",false, 1.5, "double");
        cmd.add( Q2_max );
        ValueArg<double> W_max("","Wmax","Maximal value for W in GeV (default 1.9 GeV)",false, 1.9, "double");
        cmd.add( W_max );

//        vector<double> allowed_r = {-1.,0.,1.};
//        ValuesConstraint<double> allowedVals_r( allowed_r );
//        ValueArg<double> r("r","BosonPolarization","Value for W boson polarization (-1., 0., 1.)"
//                                                   ,false, 1., &allowedVals_r);
//        cmd.add( r );
//        vector<double> allowed_lambda = {-1./2., 1./2.};
//        ValuesConstraint<double> allowedVals_lambda( allowed_lambda );
//        ValueArg<double> lambda("l","NucleonHelicity","Value for the incoming nucleon helicity "
//                                                      "(-1./2., 1./2.)", false, 1./2., &allowedVals_lambda);
//        cmd.add( lambda );

        SwitchArg Not_H_Current_CT("","CT","Do NOT use contact term for hadronic current (default off)", false);
        cmd.add( Not_H_Current_CT );

        SwitchArg H_Current_all_minus_CT("","Hall","Use all but contact terms for hadronic current (default off)", false);
        cmd.add( H_Current_all_minus_CT );

        // Parse the args.
        cmd.parse( argc, argv );

        this->process = process.getValue();
        this->n_Q2 = n_Q2.getValue();
        this->n_W = n_W.getValue();
        this->Q2_min = Q2_min.getValue();
        this->Q2_max = Q2_max.getValue();
        this->W_max = W_max.getValue();
//        this->r = r.getValue();
//        this->lambda = lambda.getValue();
        this->Not_H_Current_CT = Not_H_Current_CT.getValue();
        this->H_Current_all_except_CT = H_Current_all_minus_CT.getValue();
        this->filePath_aux = filePath.getValue() + "chi_";
        this->filePath = this->filePath_aux;
//        if(!this->Not_H_Current_CT) this->filePath += "CT_";
//        if(this->H_Current_all_except_CT) this->filePath += "AllMinusCT_";
//        this->filePath += this->process + "_r=" + ToString::to_string(this->r) + "_lambda=" + ToString::to_string(this->lambda) + ".dat";

    }catch (ArgException &e)  // catch any exceptions
	{ cerr << "error: " << e.error() << " for arg " << e.argId() << endl; }
}

int Arguments::getN_Q2() const {
    return n_Q2;
}

int Arguments::getN_W() const {
    return n_W;
}

double Arguments::getQ2_min() const {
    return Q2_min;
}

double Arguments::getQ2_max() const {
    return Q2_max;
}

double Arguments::getW_max() const {
    return W_max;
}

const std::string &Arguments::getFilePath() const {
    return filePath;
}

const std::string &Arguments::getProcess() const {
    return process;
}

double Arguments::getR() const {
    return r;
}

double Arguments::getLambda() const {
    return lambda;
}

bool Arguments::isNot_H_Current_CT() const {
    return Not_H_Current_CT;
}

bool Arguments::is_H_Current_all_except_CT() const {
    return H_Current_all_except_CT;
}

void Arguments::set_r_and_lambda(double r, double lambda) {
    this->r = r;
    this->lambda = lambda;
    this->filePath = this->filePath_aux;
    if(!this->Not_H_Current_CT) this->filePath += "CT_";
    if(this->H_Current_all_except_CT) this->filePath += "AllExceptCT_";
    this->filePath += this->process + "_r=" + ToString::to_string(this->r) + "_lambda=" + ToString::to_string(this->lambda) + ".dat";
}

