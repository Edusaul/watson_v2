//
// Created by edusaul on 14/07/18.
//

#include <gtest/gtest.h>
#include <Spinor_helicity_State.h>
#include <Spinor_helicity_barState.h>
#include <Hadronic_current_base.h>
#include <virtual_gauge_boson_helicity_states.h>
#include <Hadronic_current_CT.h>
#include "Contraction_jmu_polarization.h"
#include "Contraction_j_eps_integrated_phi.h"
#include <Parameters_GeV.h>
#include <Write_static.h>

struct integral_phi_Test : testing::Test {
    Spinor_helicity_State *u_p;
    Spinor_helicity_barState *u_pp;
    std::vector<Hadronic_current_base*> jmu;
    virtual_gauge_boson_helicity_states *epsilon_mu;
    Contraction_jmu_polarization *contraction;
    Contraction_j_eps_integrated_phi *contr_intPhi;
    std::vector<double> imputs;
    double phiMin;
    double phiMax;
    int iter;

    integral_phi_Test() {
        this->u_p = new Spinor_helicity_State();
        this->u_pp = new Spinor_helicity_barState();
        this->jmu.push_back(new Hadronic_current_CT("pp", this->u_p, this->u_pp));
        this->epsilon_mu = new virtual_gauge_boson_helicity_states;
        this->contraction = new Contraction_jmu_polarization(&this->jmu, this->epsilon_mu);
        this->contr_intPhi = new Contraction_j_eps_integrated_phi(this->contraction);

        imputs = {0.71, 1.75219*1.75219, -1., -1/2., 0.3*3.14};

        contraction->Change_other_imputs(imputs);
        contr_intPhi->Change_other_imputs(imputs);

        phiMin = 0.0;
        phiMax =2*Param::pi;
        iter = 1;
        cout<<endl<<endl;

        std::vector<std::vector<double> > data(3);
        double phi=phiMin;
        int N = 100;
        for (int i = 0; i < N; ++i) {
            data[0].push_back(phi);
            data[1].push_back(contraction->integrand(phi).real());
            data[2].push_back(contraction->integrand(phi).imag());
            phi += (phiMax - phiMin)/double(N+1);
        }

       Write_static::Write_to_file("../../Data/data3.dat", data);
    }

    ~integral_phi_Test() override {
        delete u_p;
        delete u_pp;
        delete jmu[0];
        delete epsilon_mu;
        delete contraction;
        delete contr_intPhi;
    }
};


TEST_F(integral_phi_Test, Test_integral_in_phi_of_contraction) {
    EXPECT_EQ(IntegralGauss20::integrate<complex<double>>(contraction, phiMin, phiMax, iter)
    , contr_intPhi->integrand(imputs[4]));
}

TEST_F(integral_phi_Test, Test_dependence_in_precission_of_limMax) {
    EXPECT_EQ(IntegralGauss20::integrate<complex<double>>(contraction, phiMin, 3.141, iter)
    , IntegralGauss20::integrate<complex<double>>(contraction, phiMin, 3.14159265359, iter));
}