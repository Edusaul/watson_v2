//
// Created by edusaul on 10/07/18.
//

#ifndef WATSON_V2_ARGUMENTS_H
#define WATSON_V2_ARGUMENTS_H


#include <string>

class Arguments {
protected:
    std::string filePath_aux;
    std::string filePath;
    std::string process;
    int n_Q2;
    int n_W;
    double Q2_min;
    double Q2_max;
    double W_max;
    double r;
    double lambda;
    bool Not_H_Current_CT;
    bool H_Current_all_except_CT;

public:
    Arguments(int argc, char** argv);

    const std::string &getFilePath() const;

    const std::string &getProcess() const;

    int getN_Q2() const;

    int getN_W() const;

    double getQ2_min() const;

    double getQ2_max() const;

    double getW_max() const;

    double getR() const;

    double getLambda() const;

    bool isNot_H_Current_CT() const;

    bool is_H_Current_all_except_CT() const;

    void set_r_and_lambda(double, double);
};


#endif //WATSON_V2_ARGUMENTS_H
