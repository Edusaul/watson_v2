//
// Created by eduardo on 28/06/18.
//

#ifndef WATSON_V2_CONTRACTION_JMU_POLARIZATION_H
#define WATSON_V2_CONTRACTION_JMU_POLARIZATION_H


#include <Hadronic_current_base.h>
#include <Call_Function.h>
#include <virtual_gauge_boson_helicity_states.h>
#include "IntegralGauss20.h"
#include "Wigner_D_matrices.h"

class Contraction_jmu_polarization : public  Integrable<complex<double>> {
private:
    vector<Hadronic_current_base*> *jmu;
    virtual_gauge_boson_helicity_states *epsilon_mu;
    double Q2;
    double W2;
    int r;
    double lambda;
    double theta;
    Vector4<double> p;
    Vector4<double> q;
    Vector4<double> pmes;
    Vector4<double> pp;
//    double MN;
    double MN2;
//    double mmes;
    double mmes2;

    double qv;
    double Emes;
//    double Ep;
    double pmes_v;

    double sin_theta;
    double cos_theta;

    Wigner_D_matrices *d_matrix;

public:
    Contraction_jmu_polarization(vector<Hadronic_current_base *> *jmu, virtual_gauge_boson_helicity_states *epsilon_mu);

    int Change_other_imputs(std::vector<double>);

    complex<double> integrand(double) override;

    virtual ~Contraction_jmu_polarization();

};


#endif //WATSON_V2_CONTRACTION_JMU_POLARIZATION_H
