//
// Created by edusaul on 29/06/18.
//

#include <Parameters_GeV.h>
#include "Contraction_j_eps_integrated_phi.h"

Contraction_j_eps_integrated_phi::Contraction_j_eps_integrated_phi(Contraction_jmu_polarization *jmu_eps) : jmu_eps(
        jmu_eps) {
    this->phiMin = 0.0;
    this->phiMax = 2.0*Param::pi;
    this->iter = 1;
}

Contraction_j_eps_integrated_phi::Contraction_j_eps_integrated_phi(Contraction_jmu_polarization *jmu_eps,
                                                                   int iter) : jmu_eps(jmu_eps),
                                                                                                     iter(iter) {
    this->phiMin = 0.0;
    this->phiMax = 2.0*Param::pi;
}

int Contraction_j_eps_integrated_phi::Change_other_imputs(std::vector<double> imputs) {
    this->Q2=imputs[0];
    this->W2=imputs[1];
    this->r=imputs[2];
    this->lambda=imputs[3];

    return 0;
}

complex<double> Contraction_j_eps_integrated_phi::integrand(double theta) {
    std::vector<double> imputs = {this->Q2, this->W2, this->r, this->lambda, theta};
    this->jmu_eps->Change_other_imputs(imputs);

    return IntegralGauss20::integrate<complex<double>>(this->jmu_eps,this->phiMin, this->phiMax, 1);
}






