//
// Created by edusaul on 9/07/18.
//

#ifndef WATSON_V2_WIGNER_D_MATRICES_H
#define WATSON_V2_WIGNER_D_MATRICES_H


#include <complex>
#include <src/relativistic-quantum-mechanics/Spin.h>

class Wigner_D_matrices {
private:
    std::complex<double> im;
    double sin_theta_half;
    double cos_theta_half;
    std::complex<double> exp_p_iphi;
    std::complex<double> exp_m_iphi;

public:
    void set_angles(double, double);
    std::complex<double> getFactor(Spin, Spin);


};


#endif //WATSON_V2_WIGNER_D_MATRICES_H
