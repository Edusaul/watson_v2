//
// Created by edusaul on 10/07/18.
//

#include <Parameters_GeV.h>
#include <Write_static.h>
#include <Hadronic_current_CT.h>
#include <Hadronic_current_CrLambda.h>
#include <Hadronic_current_CrSigma.h>
#include <Hadronic_current_eta.h>
#include <Hadronic_current_KP.h>
#include <Hadronic_current_Pi.h>
#include "Chi_r_lambda.h"

Chi_r_lambda::Chi_r_lambda(Arguments *parameters) : parameters(parameters) {
    this->u_p = new Spinor_helicity_State();
    this->u_pp = new Spinor_helicity_barState();
    if(!parameters->isNot_H_Current_CT())
        this->jmu.push_back(new Hadronic_current_CT(this->parameters->getProcess(), this->u_p, this->u_pp));
    if(parameters->is_H_Current_all_except_CT()) {
        this->jmu.push_back(new Hadronic_current_CrLambda(this->parameters->getProcess(), this->u_p, this->u_pp));
        this->jmu.push_back(new Hadronic_current_CrSigma(this->parameters->getProcess(), this->u_p, this->u_pp));
        this->jmu.push_back(new Hadronic_current_eta(this->parameters->getProcess(), this->u_p, this->u_pp));
        this->jmu.push_back(new Hadronic_current_KP(this->parameters->getProcess(), this->u_p, this->u_pp));
        this->jmu.push_back(new Hadronic_current_Pi(this->parameters->getProcess(), this->u_p, this->u_pp));
    }
    this->epsilon_mu = new virtual_gauge_boson_helicity_states;
    this->contraction = new Contraction_jmu_polarization(&this->jmu, this->epsilon_mu);
    this->contr_intPhi = new Contraction_j_eps_integrated_phi(this->contraction);
    this->contr_intTheta = new Contraction_j_eps_integrated_theta_and_phi(this->contr_intPhi);

    this->W_min = Param::mp + Param::mk;
}

Chi_r_lambda::~Chi_r_lambda() {
    delete u_p;
    delete u_pp;
    for (int i = 0; i < this->jmu.size(); ++i) {
        delete jmu[i];
    }
    delete epsilon_mu;
    delete contraction;
    delete contr_intPhi;
    delete contr_intTheta;
}

std::complex<double> Chi_r_lambda::getChi(double Q2, double W) {

    double W2 = W*W;
    std::vector<double>imputs = {Q2,W2,this->parameters->getR()};
    this->contr_intTheta->Change_other_imputs(imputs);

    return contr_intTheta->integrand(this->parameters->getLambda());
}

void Chi_r_lambda::writeDataToFile() {
    std::vector<double> Q2 = {this->parameters->getQ2_min()};
    std::vector<double> W = {this->W_min};
    double chi;
    std::vector<std::vector<double> > data(3);

    std::vector<double> imputs(5);

    double ff_dipole;

    for (int i = 0; i < this->parameters->getN_Q2(); ++i) {
        ff_dipole = 1.0 / (1. + Q2[i]) / (1. + Q2[i]);
        for (int j = 0; j < parameters->getN_W(); ++j) {

            chi = this->getChi(Q2[i],W[j]).real() * ff_dipole;

            data[0].push_back(Q2[i]);
            data[1].push_back(W[j]);
            data[2].push_back(chi);

            if(i==0) W.push_back(W[j] + (this->parameters->getW_max()-this->W_min)
                                        /double(this->parameters->getN_W()-1));
        }
        Q2.push_back(Q2[i] + (this->parameters->getQ2_max()-this->parameters->getQ2_min())
                /double(this->parameters->getN_Q2()-1));
    }


    Write_static::Write_to_file(this->parameters->getFilePath(), data);
}

