//
// Created by edusaul on 10/07/18.
//

#ifndef WATSON_V2_CHI_H
#define WATSON_V2_CHI_H

#include <Spinor_helicity_State.h>
#include <Spinor_helicity_barState.h>
#include <virtual_gauge_boson_helicity_states.h>
#include "Arguments.h"
#include "Contraction_jmu_polarization.h"
#include "Contraction_j_eps_integrated_phi.h"
#include "Contraction_j_eps_integrated_theta_and_phi.h"

class Chi_r_lambda {
protected:
    Arguments *parameters;
    Spinor_helicity_State *u_p;
    Spinor_helicity_barState *u_pp;
    std::vector<Hadronic_current_base*> jmu;
    virtual_gauge_boson_helicity_states *epsilon_mu;
    Contraction_jmu_polarization *contraction;
    Contraction_j_eps_integrated_phi *contr_intPhi;
    Contraction_j_eps_integrated_theta_and_phi *contr_intTheta;
    double W_min;

public:
    explicit Chi_r_lambda(Arguments *parameters);

    virtual ~Chi_r_lambda();

    std::complex<double> getChi(double, double);

    void writeDataToFile();


};


#endif //WATSON_V2_CHI_H
