//
// Created by edusaul on 29/06/18.
//

#ifndef WATSON_V2_CONTRACTION_J_EPS_INTEGRATED_PHI_H
#define WATSON_V2_CONTRACTION_J_EPS_INTEGRATED_PHI_H



#include "Contraction_jmu_polarization.h"

class Contraction_j_eps_integrated_phi : public Integrable<complex<double>>{
protected:
    Contraction_jmu_polarization *jmu_eps;
    double Q2;
    double W2;
    double r;
    double lambda;
    double phiMin;
    double phiMax;
    int iter;

public:

    explicit Contraction_j_eps_integrated_phi(Contraction_jmu_polarization *jmu_eps);

    Contraction_j_eps_integrated_phi(Contraction_jmu_polarization *jmu_eps, int iter);

    int Change_other_imputs(std::vector<double>);

    complex<double> integrand(double) override;

};


#endif //WATSON_V2_CONTRACTION_J_EPS_INTEGRATED_PHI_H
