//
// Created by eduardo on 28/06/18.
//

#include <Parameters_GeV.h>
#include "Contraction_jmu_polarization.h"

Contraction_jmu_polarization::Contraction_jmu_polarization(vector<Hadronic_current_base *> *jmu,
                                                           virtual_gauge_boson_helicity_states *epsilon_mu) : jmu(jmu),
                                                                                                              epsilon_mu(
                                                                                                                     epsilon_mu) {
    this->MN2 = Param::mp2;
    this->mmes2 = Param::mk2;

    this->d_matrix = new Wigner_D_matrices();
}

int Contraction_jmu_polarization::Change_other_imputs(std::vector<double> imputs) {
    this->r=int(imputs[2]);
    this->lambda=imputs[3];

    if(this->Q2 != imputs[0] ||  this->W2!=imputs[1]) {
        this->Q2=imputs[0];
        this->W2=imputs[1];

        this->qv = sqrt((this->MN2 * this->MN2 - 2.0 * this->MN2 * (-this->Q2 + this->W2) +
                          (-this->Q2 - this->W2) * (-this->Q2 - this->W2)) / (4.0 * this->W2));
        double q0 = sqrt(-this->Q2 + this->qv * this->qv);
        double E = sqrt(this->MN2 + this->qv * this->qv);
        this->q.SetP4(q0, 0, 0, this->qv);
        this->p.SetP4(E, 0, 0, -this->qv);

        this->pmes_v = sqrt((this->MN2 * this->MN2 - 2.0 * this->MN2 * (this->mmes2 + this->W2) +
                          (this->mmes2 - this->W2) * (this->mmes2 - this->W2)) / (4.0 * this->W2));
        this->Emes = sqrt(this->mmes2 + this->pmes_v*this->pmes_v);
    }

    if(this->theta != imputs[4]){
        this->theta=imputs[4];
        this->sin_theta = sin(this->theta);
        this->cos_theta = cos(this->theta);
    }

    return 0;
}

complex<double> Contraction_jmu_polarization::integrand(double phi) {

    this->pmes.SetP4(this->Emes, this->pmes_v * this->sin_theta * cos(phi),
                     this->pmes_v * this->sin_theta * sin(phi), this->pmes_v * this->cos_theta);
    this->pp = this->q + this->p - this->pmes;

    for (int i = 0; i < this->jmu->size(); ++i) {
        (*this->jmu)[i]->set_pp_SpinorAngles(this->theta,phi);
        (*this->jmu)[i]->setMomenta(this->p, this->pp, this->q, this->pmes);
    }


    this->d_matrix->set_angles(this->theta,phi);

    Tensor<complex<double>> aux_tensor;
    aux_tensor.SetRank(1);
    Matrix<Tensor<complex<double>>> jmu_sum(1,1,aux_tensor);
    jmu_sum.Zero();

    Matrix<complex<double> > j_eps_contraction(1,1);
    j_eps_contraction.Zero();

    // here the hadronic current has the form \bar(u)(pp, \rho) j^\mu u(p, \lambda) where \rho
    // is the helicity of the outcoming nucleon and \lambda is the helicity of the incoming one.

    for (Spin rho = -1 / 2.; rho <= 1 / 2.; rho++) {
        //here there should be the CG coefficient, but in our case it is 1, so it is ommited!!!!
        for (int i = 0; i < this->jmu->size(); ++i) {
            jmu_sum += (*this->jmu)[i]->getJmu(this->lambda, rho) * d_matrix->getFactor(this->r-this->lambda,-rho);
        }
        if(this->r==0) j_eps_contraction += this->epsilon_mu->getVector_of_helicity_state_r0(this->qv, this->Q2)*jmu_sum;
        else if(this->r==1) j_eps_contraction += this->epsilon_mu->getVector_of_helicity_state_rp1()*jmu_sum;
        else if(this->r==-1) j_eps_contraction += this->epsilon_mu->getVector_of_helicity_state_rm1()*jmu_sum;
        else cout << "ERROR : not a valid value for r helicity !!!!";

        jmu_sum.Zero();
    }

//    cout<<theta<<"  "<<phi<<"   "<<j_eps_contraction<<endl;

    //the extra sin(theta) comes from the jacobian, we integrate theta instead of cos(theta)
    complex<double> aux = j_eps_contraction*this->sin_theta;
//    cout<<phi<<"   "<<aux<<endl;
    return aux;
}

Contraction_jmu_polarization::~Contraction_jmu_polarization() {
    delete this->d_matrix;
}
