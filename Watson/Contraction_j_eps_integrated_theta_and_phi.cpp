//
// Created by edusaul on 29/06/18.
//

#include <Parameters_GeV.h>
#include "Contraction_j_eps_integrated_phi.h"
#include "Contraction_j_eps_integrated_theta_and_phi.h"

Contraction_j_eps_integrated_theta_and_phi::Contraction_j_eps_integrated_theta_and_phi(
        Contraction_j_eps_integrated_phi *jmu_eps_integrated_phi) : jmu_eps_integrated_phi(jmu_eps_integrated_phi) {
    this->thetaMin = 0.0;
    this->thetaMax = Param::pi;
    this->iter = 1;
}

Contraction_j_eps_integrated_theta_and_phi::Contraction_j_eps_integrated_theta_and_phi(
        Contraction_j_eps_integrated_phi *jmu_eps_integrated_phi, int iter) : jmu_eps_integrated_phi(
        jmu_eps_integrated_phi), iter(iter) {
    this->thetaMin = 0.0;
    this->thetaMax = Param::pi;
}


int Contraction_j_eps_integrated_theta_and_phi::Change_other_imputs(std::vector<double> imputs) {
    this->Q2=imputs[0];
    this->W2=imputs[1];
    this->r=imputs[2];

    return 0;
}

complex<double> Contraction_j_eps_integrated_theta_and_phi::integrand(double lambda) {
    std::vector<double> imputs = {this->Q2, this->W2, this->r, lambda};
    this->jmu_eps_integrated_phi->Change_other_imputs(imputs);

    return IntegralGauss20::integrate<complex<double>>(this->jmu_eps_integrated_phi, this->thetaMin, this->thetaMax, this->iter);
}



