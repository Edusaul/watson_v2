//
// Created by eduardo on 27/06/18.
//

#include <iostream>
#include "Arguments.h"
#include "Chi_r_lambda.h"
#include <Measure_time.h>

int main(int argc, char** argv) {
    Measure_time time;

#pragma omp parallel for
    for (int r = -1; r <= 1; ++r) {
        Arguments parameters(argc, argv);
        for (double lambda = -1./2.; lambda <= 1./2.; ++lambda) {
            parameters.set_r_and_lambda(double(r), lambda);
            Chi_r_lambda chi(&parameters);
            chi.writeDataToFile();
        }
    }

    time.getTime();

    return 0;
}