//
// Created by edusaul on 9/07/18.
//

#include <cmath>
#include "Wigner_D_matrices.h"

void Wigner_D_matrices::set_angles(double theta, double phi) {
    this->im = {0.,1.};
    this->sin_theta_half = sin(theta/2.);
    this->cos_theta_half = cos(theta/2.);
    this->exp_p_iphi = exp(im * phi);
    this->exp_m_iphi = exp(-im * phi);
}

std::complex<double> Wigner_D_matrices::getFactor(Spin m, Spin mp) {
    std::complex<double> d;

//    cout<<"calling..............................."<<endl;

//    cout<<endl<<"->"<<m.Value()<<"   "<<mp.Value()<<"  ";

    if(m.Value() == 1./2. && mp.Value() == -1./2.) {
//        cout<<" case1: +-";
        d = -this->exp_m_iphi * this->sin_theta_half;
    } else if(m.Value() == -1./2. && mp.Value() == 1./2.){
//        cout<<" case2: -+";
        d = this->exp_p_iphi * this->sin_theta_half;
    } else if(m.Value() == 1./2. && mp.Value() == 1./2. ||
              m.Value() == -1./2. && mp.Value() == -1./2.) {
//        cout<<" case3: ++ --";
        d = this->cos_theta_half;
    } else d=0.0;
//        cout<<endl<<endl<<"ERROR: wrong value for D matrix  ";
//cout<<endl<<"ending........................................."<<endl;

//    if(mp.Value() == 1./2.) {
//        if(m.Value() == -1./2.) d = -this->exp_m_iphi * this->sin_theta_half;
//    } else if(m.Value() == 1./2.) d = this->exp_p_iphi * this->sin_theta_half;
//    else d = this->cos_theta_half;

    return d;
}

