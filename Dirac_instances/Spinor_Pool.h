//
// Created by edusaul on 25/05/18.
//

#ifndef WATSON_V2_SPINOR_POOL_H
#define WATSON_V2_SPINOR_POOL_H


#include "relativistic-quantum-mechanics.h"
#include <list>

class Spinor_Pool {
private:
    Spinor_Pool();

    std::list<DiracSpinor*> resources;

    static Spinor_Pool *instance;

public:
    static Spinor_Pool *get_instance();

    DiracSpinor *get_Spinor();

    void return_Spinor(DiracSpinor *spinor);

    virtual ~Spinor_Pool();
};


#endif //WATSON_V2_SPINOR_POOL_H
