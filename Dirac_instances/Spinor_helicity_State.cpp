//
// Created by eduardo on 13/06/18.
//

#include "Spinor_helicity_State.h"

//For this spinors the imput is not the spin, but the helicity !!!!!!

void Spinor_helicity_State::setAngles(double theta, double phi) {
    this->sin_theta_half = sin(theta/2.0);
    this->cos_theta_half = cos(theta/2.0);
    this->phi = phi;
    this->exp_p_iphi = exp(this->im * this->phi);
    this->exp_m_iphi = exp(-this->im * this->phi);
}

void Spinor_helicity_State::SetP4(const Vector4<double> &__p4, const double &__mass) {

    this->Zero();
    _p4 = __p4;
    _mass = __mass;
    if(_spin == 1/2.){ // spin-1/2 case
        complex<double> norm,epm;
//        Matrix<complex<double> > sigP(2,2);
//        Matrix<complex<double> > chi(2,1);
//        PauliSigma sigma;

        norm = sqrt(__p4.E() + __mass);
        epm = __p4.E() + __mass;
//        sigP = sigma(1)*__p4.X() + sigma(2)*__p4.Y() + sigma(3)*__p4.Z();
        double pv_modulus = sqrt(__p4.X()*__p4.X() + __p4.Y()*__p4.Y() + __p4.Z()*__p4.Z());


        // set spin up
//        chi(0,0) = 1.;
//        chi(1,0) = 0.;

        _spinors[1](0,0) = this->sin_theta_half * norm ;
        _spinors[1](1,0) = -this->exp_p_iphi * this->cos_theta_half * norm ;
        _spinors[1](2,0) = this->sin_theta_half * pv_modulus/epm * norm;
        _spinors[1](3,0) = -this->exp_p_iphi * this->cos_theta_half * pv_modulus/epm * norm;

        // set spin down
//        chi(0,0) = 0.;
//        chi(1,0) = 1.;
        _spinors[0](0,0) = -this->exp_m_iphi * this->cos_theta_half * norm;
        _spinors[0](1,0) = this->sin_theta_half * norm;
        _spinors[0](2,0) = this->exp_m_iphi * this->cos_theta_half * pv_modulus/epm * norm;
        _spinors[0](3,0) = this->sin_theta_half * pv_modulus/epm * norm;
    }
    else { // higher spins
        Spin j = _spin - 1/2.;
        DiracSpinor_modified u; // spin-1/2
        PolVector eps(j);
        u.SetP4(__p4,__mass);
        eps.SetP4(__p4,__mass);
        for(Spin mf = -1/2.; mf <= 1/2.; mf++){
            for(Spin mb = -j; mb <= j; mb++){
                for(int i = 0; i < (int)(2*_spin + 1); i++){
                    Spin mz = i - _spin;
                    _spinors[i] += u(mf)*Clebsch(j,mb,1/2.,mf,_spin,mz)*eps(mb);
                }
            }
        }
    }
    this->_SetProjector();
}

Spinor_helicity_State::Spinor_helicity_State(double theta, double phi) : phi(phi) {
    this->im = {0.,1.};

    this->setAngles(theta,phi);
}

Spinor_helicity_State::Spinor_helicity_State() {
    this->im = {0.,1.};

    double theta = 0.0;
    this->phi = 0.0;

    this->setAngles(theta,phi);
}





