//
// Created by eduardo on 24/05/18.
//

#ifndef WATSON_V2_DIRACGAMMA_MU_H
#define WATSON_V2_DIRACGAMMA_MU_H


#include "relativistic-quantum-mechanics.h"

class DiracGamma_mu {
private:
    DiracGamma_mu();
    DiracGamma gmu;
    DiracGamma5 g5;
    Matrix<Tensor<complex<double> > > gmu_x_g5;
    static DiracGamma_mu *instance;
    DiracSigma sigma;

public:
    static DiracGamma_mu *get_instance();

    static DiracGamma get_gmu();

    static DiracGamma5 get_g5();

    static Matrix<Tensor<complex<double> > > get_gmu_x_g5();

    static DiracSigma get_sigma();

};


#endif //WATSON_V2_DIRACGAMMA_MU_H
