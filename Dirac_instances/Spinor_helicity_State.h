//
// Created by eduardo on 13/06/18.
//

#ifndef WATSON_V2_SPINOR_BARSTATE_H
#define WATSON_V2_SPINOR_BARSTATE_H

#include "DiracSpinor_modified.h"

//For this spinors the imput is not the spin, but the helicity !!!!!!

class Spinor_helicity_State : public DiracSpinor_modified {
protected:
    // attributes:
    double sin_theta_half;
    double cos_theta_half;
    double phi;
    std::complex<double> exp_p_iphi;
    std::complex<double> exp_m_iphi;
    std::complex<double> im;

public:
//    typedef Matrix<Tensor<complex<double>>> MatrizTensorComplejo;

    Spinor_helicity_State(double theta, double phi);

    Spinor_helicity_State();

    void setAngles(double theta, double phi) override;

    void SetP4(const Vector4<double> &__p4,
               const double &__mass) override;

};


#endif //WATSON_V2_SPINOR_BARSTATE_H
