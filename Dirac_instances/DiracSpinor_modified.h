//
// Created by eduardo on 27/06/18.
//

#ifndef WATSON_V2_DIRACSPINOR_MODIFIED_H
#define WATSON_V2_DIRACSPINOR_MODIFIED_H

#include <iostream>
#include <complex>
#include <src/relativistic-quantum-mechanics/DiracSpinor.h>

using namespace std;

class DiracSpinor_modified {
protected:
  // attributes:

  /// Spinors, _spinors[n] = \f$ u_{\mu_1\mu_2\ldots\mu_{S-1/2}}(P,n-S) \f$
  vector<Matrix<Tensor<complex<double> > > > _spinors;
  Vector4<double> _p4; ///< The particle's 4-momentum
  double _mass; ///< The particle's mass
  Spin _spin; ///< Spin of the particle
  Matrix<Tensor<complex<double> > > _projector; ///< Spin S Projection Operator

  // private functions:

  /** Initialize for spin */
  void _Init(const Spin &__spin);

  /** Copy @a dspin to @a this */
  void _Copy(const DiracSpinor_modified &__dspin);

  /** Set the spin-S projection operator.
   *
   * For a spin-1/2 particle, the projector is set to be,
   * \f[ P^{(\frac{1}{2})}(P) = \frac{1}{2w}(\gamma^{\mu}P_{\mu} + w) \f]
   * where \f$P\f$ is the 4-momentum, \f$ w \f$ is the mass,
   * and \f$M = \pm \frac{1}{2} \f$ is the spin projection.
   *
   * For higher spins, the projector is calculated using,
   * \f[ p^{(S)}_{\mu_1\mu_2\ldots\mu_n\nu_1\nu_2\ldots\nu_n}(P)
   * = \frac{1}{2w}\sum\limits_M u_{\mu_1\mu_2\ldots\mu_n}(P,M)
   * \bar{u}_{\nu_1\nu_2\ldots\nu_n}(P,M)\f]
   *
   * where \f$ n = S - 1/2 \f$.
   *
   */
  void _SetProjector();

public:

  // create/copy/destroy:

  /// Default Constructor (spin 1/2)
  DiracSpinor_modified(){
    this->_Init(Spin(1,2));
    _mass = 0.;
  }

  /// Constructor
  DiracSpinor_modified(const Spin &__spin){
    this->_Init(__spin);
    _mass = 0.;
  }

  /// Copy Constructor
  DiracSpinor_modified(const DiracSpinor_modified &__dspin){
    this->_Copy(__dspin);
  }

  /** Destructor */
  virtual ~DiracSpinor_modified(){}

  // Setters:

  virtual void setAngles(double theta, double phi){};

  /// Set the spin of the spinor
  void SetSpin(const Spin &__spin){
    this->Clear();
    this->_Init(__spin);
    _mass = 0.;
  }

    /** Set the DiracSpinor_modified object for a given 4-momentum.
   *
   * @param p4 The particle's 4-momentum
   * @param mass Pole mass (used for normalization)
   *
   * For spin-1/2 particles, the spinors are defined as,
   * \f[ u(P,M) = \sqrt{E+w}\left( \begin{array}{c} \chi(M) \\
   * \frac{\vec{\sigma}\cdot\vec{P}}{E + w} \chi(M) \end{array}  \right) \f]
   * where \f$P,M \f$ are the 4-momentum,spin projection of the particle and
   * \f[ \chi(+\frac{1}{2}) = \left(\begin{array}{c}1\\0\end{array}\right)
   * \qquad \chi(-\frac{1}{2}) =\left(\begin{array}{c}0\\1\end{array}\right)\f]
   *
   * Higher spin spinors are built from tensor products of the spin-1/2 spinors
   * and the spin-(n = S-1/2) polarization vectors according to,
   * \f[u_{\mu_1\mu_2\ldots\ldots\mu_n}(P,M) = \sum\limits_{m_nm_{\frac{1}{2}}}
   * (nm_n\frac{1}{2}m_{\frac{1}{2}}|SM)\epsilon_{\mu_1\mu_2\ldots\ldots\mu_n}
   * (P,m_n)u(P,m_{\frac{1}{2}})\f]
   * where \f$(nm_n\frac{1}{2}m_{\frac{1}{2}}|SM)\f$ are the Clebsch-Gordon
   * coefficents coupling spin-(n = S-1/2) and spin-1/2 to spin-S
   * (see Clebsch()).
   */
    virtual void SetP4(const Vector4<double> &__p4,
	     const double &__mass);

  // Getters:

  /** Returns \f$u_{\mu_1\mu_2\ldots\mu_{S-1/2}}(p,m_z)\f$
   *
   * @param mz Spin projection along the z-axis (\f$ -S \leq m_z \leq S\f$)
   */
  virtual const Matrix<Tensor<complex<double> > >& operator()(const Spin &__mz) const {
    int i = (int)(_spin + __mz);
    if(i < 0 || i > 2*_spin) {
      cout << "Error! Attempt to access non-existent spinor (mz out of range)."
	   << endl;
    }
    assert(i >= 0 && i <= (int)(2*_spin));
    return _spinors[i];
  }

  /** Returns the spin projection operator for spin S
   *
   * The spin projection operator is defined as,
   * \f[ P^{(S)}_{\mu_1\mu_2\ldots\mu_{S-1/2}\nu_1\nu_2\ldots\nu_{S-1/2}}(P)
   * = \frac{1}{2w} \sum\limits_{M} u_{\mu_1\mu_2\ldots\mu_{S-1/2}}(P,M)
   * \bar{u}_{\nu_1\nu_2\ldots\nu_{S-1/2}}(P,M) \f]
   * where \f$P,M\f$ are the 4-momentum, spin projection and
   * \f$-S\leq M \leq S\f$.
   *
   * <b> Example: </b>
   *
   * \f[ P^{(\frac{3}{2})}_{\mu\nu}(P) = \sum\limits_{M}u_{\mu}(P,M)
   * \bar{u}_{\nu}(P,M) = \frac{1}{2w}(P_{\rho}\gamma^{\rho}+w)\left[-
   * \tilde{g}_{\mu\nu} + \frac{1}{3}\tilde{g}_{\mu\alpha}\gamma^{\alpha}
   * \tilde{g}_{\nu\beta}\gamma^{\beta}\right] \f]
   * where \f$\tilde{g}_{\mu\nu} = g_{\mu\nu} - \frac{P_{\mu}P_{\nu}}{w^2} \f$
   * and \f$w\f$ is the mass.
   *
   * See SetProjector() for details on how this is calculated.
   */
  virtual const Matrix<Tensor<complex<double> > >& Projector() const {
    return _projector;
  }

  /** Returns the Feynman propagator for spin S
   *
   * \returns \f[\frac{2w P^{(S)}_{\mu_1\mu_2\ldots\mu_{S-1/2}
   *                           \nu_1\nu_2\ldots\nu_{S-1/2}}(P)}{P^2 - w^2}\f]
   *
   * with \f$w\rightarrow\f$_mass, \f$P\rightarrow\f$_p4 and
   * \f$P^{(S)}\rightarrow\f$Projector().
   *
   * Note: This is slow, for speed use Projector() and do the division after
   *       any matrix/tensor operations.
   */
  virtual Matrix<Tensor<complex<double> > > Propagator() const {
    Matrix<Tensor<complex<double> > > prop(4,4);
    prop = (this->Projector()*2.*_mass)/(_p4.M2() - _mass*_mass);
    return prop;
  }

  /** Returns the Breit-Wigner propagator for spin S
   *
   * @param width Breit-Wigner width.
   * @returns \f[ \frac{2w P^{(S)}_{\mu_1\mu_2\ldots\mu_{S-1/2}
   *        \nu_1\nu_2\ldots\nu_{S-1/2}}(P) w\Gamma}{P^2 - w^2 + iw\Gamma}\f]
   * with \f$\Gamma\rightarrow\f$width, \f$w\rightarrow\f$_mass,
   * \f$P\rightarrow\f$_p4 and \f$P^{(S)}\rightarrow\f$Projector().
   *
   * Note: This is slow, for speed use Projector() and do the division after
   *       any matrix/tensor operations.
   */
  virtual Matrix<Tensor<complex<double> > > PropagatorBW(const double &__width) const {
    Matrix<Tensor<complex<double> > > prop(4,4);
    prop = (this->Projector()*2.*_mass)*BreitWigner(_p4,_mass,__width);
    return prop;
  }

  /// Return the particle's 4-momentum
  inline const Vector4<double>& GetP4() const {
    return _p4;
  }

  /// Return the particle's mass
  inline double GetMass() const {
    return _mass;
  }

  // Functions:

  /** Boost the spinor.
   *
   * The boost vector is given by \f$\vec{\beta} = (bx,by,bz)\f$
   *
   * For spin-1/2 particles,
   * boosting is done by constructing the boost operator for spinors,
   * \f[ D(\vec{\beta}) = \left(\begin{array}{cc} cosh(\alpha/2) &
   * \vec{\sigma}\cdot\hat{P} sinh(\alpha/2) \\ \vec{\sigma}\cdot\hat{P}
   * sinh(\alpha/2) & cosh(\alpha/2) \end{array} \right) \f]
   * where \f$ tanh(\alpha) = \beta \f$. Then the boosted spinor is given by,
   * \f[ u(P,M) = D(\vec{\beta}) u(P',M) \f]
   * where \f$P(P')\f$ is the momentum in the boosted(original) frame.
   *
   * For higher spins, a spin-1/2 spinor, \f$u(P',M)\f$, and a
   * spin-(n = S - 1/2) polarization vector,
   * \f$\epsilon_{\mu_1\mu_2\ldots\mu_n}(P',M) \f$, are constructed in the
   * current frame. These are then boosted using the
   * boost vector \f$\vec{\beta}\f$ and the spin-S spinor is reconstructed out
   * of the boosted spinor and polarization vector according to the equation
   * given in SetP4().
   */
  virtual void Boost(const double &__bx,
	     const double &__by,
	     const double &__bz);

  /// Boost the spinor to p4's rest frame (see Boost(double,double,double)).
  virtual void Boost(const Vector4<double> &__p4) {
    double p = __p4.P();
    this->Boost(-__p4.X()/p,-__p4.Y()/p,-__p4.Z()/p);
  }

  /// Clear the DiracSpinor object
  virtual void Clear() {
    if(!_spinors.empty()) _spinors.clear();
    _spin = 0;
  }

  /// Zero the spinors only.
  virtual void Zero() {
    int size = (int)_spinors.size();
    for(int i = 0; i < size; i++) _spinors[i].Zero();
  }

  /// Returns \f$u_{\mu_1\mu_2\ldots\mu_{S-1/2}}(P,\lambda) \f$
  virtual Matrix<Tensor<complex<double> > > Helicity(const Spin &__lam) const {
    Matrix<Tensor<complex<double> > > hel(4,1);
    SetRank(hel,(int)(_spin - 1/2.));
    hel.Zero();
    for(Spin m = -_spin; m <= _spin; m++){
      hel += Wigner_D(_p4.Phi(),_p4.Theta(),0.,_spin,m,__lam)
	*_spinors[(int)(_spin + m)];
    }
    return hel;
  }

  // static functions:

  /** Returns the spin-<em>j</em> projector as a 2 x @a rank tensor.
   *
   * @param j Spin of the projector
   * @param rank Tensor rank of projector will be 2 x rank
   * @param p4 4-momentum
   * @param mass Mass
   * @param projector Will be set to the projector
   *
   * Builds the spin-<em>j</em> projection operator in @a rank space.
   */
  static void Projector(const Spin &__j,int __rank,
			const Vector4<double> &__p4,
			const double &__mass,
			Matrix<Tensor<complex<double> > > &__projector);

};


#endif //WATSON_V2_DIRACSPINOR_MODIFIED_H
