//
// Created by eduardo on 27/06/18.
//

#ifndef WATSON_V2_VIRTUAL_GAUGE_BOSON_HELICITY_STATES_H
#define WATSON_V2_VIRTUAL_GAUGE_BOSON_HELICITY_STATES_H


#include <complex>

class virtual_gauge_boson_helicity_states {
private:
    std::complex<double> im;
    Vector4<complex<double> > vector_of_helicity_state_r0;
    Vector4<complex<double> > vector_of_helicity_state_rp1;
    Vector4<complex<double> > vector_of_helicity_state_rm1;
public:
    virtual_gauge_boson_helicity_states();

    Vector4<complex<double> > &getVector_of_helicity_state_r0(double, double);
    Vector4<complex<double> > &getVector_of_helicity_state_rp1();
    Vector4<complex<double> > &getVector_of_helicity_state_rm1();
};


#endif //WATSON_V2_VIRTUAL_GAUGE_BOSON_HELICITY_STATES_H
