//
// Created by eduardo on 27/06/18.
//

#include "relativistic-quantum-mechanics.h"
#include "virtual_gauge_boson_helicity_states.h"

Vector4<complex<double> > &virtual_gauge_boson_helicity_states::getVector_of_helicity_state_r0(double p, double Q2) {

    if(p*p < Q2) cout << "p must be greater than Q2 !!!" << endl;

    this->vector_of_helicity_state_r0.SetP4(p/sqrt(Q2), 0., 0., sqrt((p*p-Q2)/Q2));
    return this->vector_of_helicity_state_r0;
}

Vector4<complex<double> > &virtual_gauge_boson_helicity_states::getVector_of_helicity_state_rp1() {

    this->vector_of_helicity_state_rp1.SetP4(0., -1.0/sqrt(2.0), -this->im/sqrt(2.0), 0.);
    return this->vector_of_helicity_state_rp1;
}

Vector4<complex<double> > &virtual_gauge_boson_helicity_states::getVector_of_helicity_state_rm1() {

    this->vector_of_helicity_state_rm1.SetP4(0., 1.0/sqrt(2.0), -this->im/sqrt(2.0), 0.);
    return this->vector_of_helicity_state_rm1;
}

virtual_gauge_boson_helicity_states::virtual_gauge_boson_helicity_states() {
    this->im = {0.,1.};
}
