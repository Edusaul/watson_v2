//
// Created by edusaul on 25/05/18.
//

#include "Spinor_Pool.h"

Spinor_Pool *Spinor_Pool::instance = 0;

Spinor_Pool::Spinor_Pool() {}

Spinor_Pool *Spinor_Pool::get_instance() {
    if (instance == 0)
        instance = new Spinor_Pool();
    return instance;
}

DiracSpinor *Spinor_Pool::get_Spinor() {
    if (Spinor_Pool::get_instance()->resources.empty()) {
        //std::cout << "Creating new." << std::endl;
        return new DiracSpinor;
    } else {
        //std::cout << "Reusing existing." << std::endl;
        DiracSpinor *spinor = Spinor_Pool::get_instance()->resources.front();
        Spinor_Pool::get_instance()->resources.pop_front();
        return spinor;
    }

}

void Spinor_Pool::return_Spinor(DiracSpinor *spinor) {
    //spinor->Clear();
    Spinor_Pool::get_instance()->resources.push_back(spinor);
}

Spinor_Pool::~Spinor_Pool() {
    while(!Spinor_Pool::get_instance()->resources.empty())
    {
        DiracSpinor *obj = Spinor_Pool::get_instance()->resources.front();
        Spinor_Pool::get_instance()->resources.pop_front();
        delete obj;
    }
}




