//
// Created by edusaul on 9/07/18.
//

#ifndef WATSON_V2_SPINOR_HELICITY_BARSTATE_H
#define WATSON_V2_SPINOR_HELICITY_BARSTATE_H

//For this spinors the imput is not the spin, but the helicity !!!!!

#include "Spinor_helicity_State.h"

class Spinor_helicity_barState : public Spinor_helicity_State{

public:
    Spinor_helicity_barState(double theta, double phi);

    Spinor_helicity_barState();

    void SetP4(const Vector4<double> &__p4,
               const double &__mass) override;
};


#endif //WATSON_V2_SPINOR_HELICITY_BARSTATE_H
