//
// Created by eduardo on 24/05/18.
//

#include "DiracGamma_mu.h"

DiracGamma_mu *DiracGamma_mu::instance = 0;

DiracGamma_mu::DiracGamma_mu() {
    this->gmu_x_g5 = this->gmu * this->g5;
}

DiracGamma_mu *DiracGamma_mu::get_instance() {
    if (instance == 0)
        instance = new DiracGamma_mu();
    return instance;
}

DiracGamma DiracGamma_mu::get_gmu() {
    return DiracGamma_mu::get_instance()->gmu;
}

DiracGamma5 DiracGamma_mu::get_g5() {
    return DiracGamma_mu::get_instance()->g5;
}

Matrix<Tensor<complex<double> > > DiracGamma_mu::get_gmu_x_g5() {
    return DiracGamma_mu::get_instance()->gmu_x_g5;
}

DiracSigma DiracGamma_mu::get_sigma() {
    return DiracGamma_mu::get_instance()->sigma;
}


